    /*----------------------------------------
                        Preloader
    ------------------------------------------*/
    // $('.js-preloader').preloadinator({
    //     minTime: 2000,
    //     scroll: false

    // });

    $(document).ready(function() {
        /*----------------------------------------
                            Tooltip
        ------------------------------------------*/

        $('[data-toggle="tooltip"]').tooltip();



        /*----------------------------------------
          Scroll to top
  ----------------------------------------*/
        function BackToTop() {
            $('.scrolltotop').on('click', function() {
                $('html, body').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });

            $(document).scroll(function() {
                var y = $(this).scrollTop();
                if (y > 600) {
                    $('.scrolltotop').fadeIn();
                } else {
                    $('.scrolltotop').fadeOut();
                }
            });
            $(document).scroll(function() {
                var m = $(this).scrollTop();
                if (m > 400) {
                    $('.chat-popup').fadeIn();
                } else {
                    $('.chat-popup').fadeOut();
                }
            });
        }
        BackToTop();

        $(window).scroll(function() {
            var scroll = $(window).scrollTop();

            if (scroll >= 100) {
                $(".header-top-section").addClass("header-top-none");
            } else {
                $(".header-top-section").removeClass("header-top-none");
            }
        });




        /* ----------------------------------------
              Hide Show Header on Scroll
        ------------------------------------------ */
        function HideShowHeader() {

            var didScroll;
            var lastScrollTop = 0;
            var delta = 50;
            var navbarHeight = 75;
            var navbarHideAfter = navbarHeight

            $(window).scroll(function(event) {
                didScroll = true;
            });

            if ($('.scroll-hide').length > 0) {

                setInterval(function() {
                    if (didScroll) {
                        hasScrolled();
                        didScroll = false;
                    }
                }, 100);
            }
            return false;

            function hasScrolled() {
                var st = $(this).scrollTop();

                if (Math.abs(lastScrollTop - st) <= delta)
                    return;

                if (st > lastScrollTop && st > navbarHideAfter) {
                    if ($('.scroll-hide').length > 0) {
                        $('header').addClass('hide');
                    }
                } else {
                    if ($('.scroll-hide').length > 0) {
                        if (st + $(window).height() < $(document).height()) {
                            $('header').removeClass('hide');
                            $('.header.transparent').addClass('scroll');
                        }
                    }

                    if ($(window).scrollTop() < 300) {
                        $('.header.transparent').removeClass('scroll');
                    }
                }

                lastScrollTop = st;
            }
        }
        HideShowHeader();

        /* -------------------------------------
                   Responsive menu
           -------------------------------------- */
        var siteMenuClone = function() {

            $('.js-clone-nav').each(function() {
                var $this = $(this);
                $this.clone().attr('class', 'site-nav-wrap').appendTo('.site-mobile-menu-body');
            });

            setTimeout(function() {

                var counter = 0;
                $('.site-mobile-menu .has-children').each(function() {
                    var $this = $(this);

                    $this.prepend('<span class="arrow-collapse collapsed">');

                    $this.find('.arrow-collapse').attr({
                        'data-toggle': 'collapse',
                        'data-target': '#collapseItem' + counter,
                    });

                    $this.find('> ul').attr({
                        'class': 'collapse',
                        'id': 'collapseItem' + counter,
                    });

                    counter++;

                });

            }, 1000);

            $('body').on('click', '.js-menu-toggle', function(e) {
                var $this = $(this);
                e.preventDefault();

                if ($('body').hasClass('offcanvas-menu')) {
                    $('body').removeClass('offcanvas-menu');
                    $this.removeClass('active');
                } else {
                    $('body').addClass('offcanvas-menu');
                    $this.addClass('active');
                }
            })

        };
        siteMenuClone();

        /* ----------------------------------------
              Counter animation
        ------------------------------------------*/
        // $('.counter-text').appear(function () {
        //     var element = $(this);
        //     var timeSet = setTimeout(function () {
        //         if (element.hasClass('counter-text')) {
        //             element.find('.counter-value').countTo();
        //         }
        //     });
        // });

        /* ----------------------------------------
                     Hero slider 
               ------------------------------------------*/
        $(".hero_slider").owlCarousel({
            items: 1,
            nav: true,
            dot: false,
            autoplay: true,
            autoplayTimeout: 4500,
            loop: false,
            margin: 30,
            navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
            smartSpeed: 2000,
          


        });
        /*-------------------------------------------
                Count Down Timer
        ---------------------------------------------*/
        // $('[data-countdown]').each(function () {
        //     var $this = $(this),
        //         finalDate = $(this).data('countdown');
        //     $this.countdown(finalDate, function (event) {
        //         $this.html(event.strftime('<span class="cdown day"><span class="time-count">%-D</span> <p>Days</p></span> <span class="cdown hour"><span class="time-count">%-H</span> <p>Hours</p></span> <span class="cdown minutes"><span class="time-count">%M</span> <p>mins</p></span> <span class="cdown second"><span class="time-count">%S</span> <p>secs</p></span>'));
        //     });
        // });




        /*---------------------------------
                   Nice select
        -----------------------------------*/
        $('select').niceSelect();




    });